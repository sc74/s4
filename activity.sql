INSERT INTO artists (name)
VALUES ("Carpenters"), 
	("Crumb"), 
	("BGYO"), 
	("Beatles"), 
	("Metallica");

INSERT INTO albums (album_name, year, artist_id)
VALUES ('Close to You', '1970-01-01', 13), 
	('Ice Melt', '2019-01-01', 14), 
	('The Light', '2021-01-01', 15), 
	('Abbey Road', '1969-01-01', 16), 
	('Master of Puppets', '1986-01-01', 17);

INSERT INTO songs (title, length, genre, album_id)
	VALUES ("Help", 302, "Pop", 17), 
	("Love", 213, "Pop", 17),
	("Reasons", 313, "Pop", 17),
	("Up Down", 204, "Pop", 18),
	("Gone", 214, "Pop", 18),
	("Retreat", 254, "Pop", 18),
	("Light", 234, "Pop", 19),
	("Baddest", 254, "Pop", 19),
	("Runnin", 254, "Pop", 19),
	("Somethin", 234, "Rock", 20),
	("Darlin", 254, "Rock", 20),
	("I want you", 214, "Rock", 20),
	("Battery", 234, "Heavy Metal", 21),
	("Orion", 254, "Heavy Metal", 21),
	("Damage", 254, "Heavy Metal", 21);	


	SELECT album_name, title, length FROM songs JOIN albums ON albums.id = songs.album_id;

	SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_name LIKE '%A%';

	SELECT * FROM albums ORDER BY album_name DESC LIMIT 4;